import React from 'react'
import './Trash.scss'
import Card from '../../components/Card/Card'

const Trash = ({
                   items,
                   saveThisItem,
                   openModal,
                   addToFavourite,
                   favourites,
               }) => {
    
    if (!JSON.parse(localStorage.getItem('Cart'))) {
        return <div className='container'>Empty</div>
    }
    
    const cart = JSON.parse(localStorage.getItem('Cart'))
    const keys = Object.keys(cart)
    const render = items.filter((item) => {
        return keys.includes(item.article.toString())
    })

    return (
        <div className='container'>
            {render.map((item) => {
                return (
                    <Card
                        favourite={favourites.includes(item.article)}
                        key={item.article}
                        item={item}
                        saveThisItem={saveThisItem}
                        openModal={openModal}
                        addToFavourite={addToFavourite}
                        fromTrash
                    />
                )
            })}
        </div>
    )
}

export default Trash
