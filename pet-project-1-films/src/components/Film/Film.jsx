import React, { useState } from "react"
import "./Film.scss"
import FilmDetails from "../FilmDetails/FilmDetails"

const Film = (props) => {
    const { film } = props
    const [expanded, setExpanded] = useState(false)
    
    const expandFilm = () => {
        setExpanded(true)
    }
    
    const hideFilm = () => {
        setExpanded(false)
    }
    
    return (
        <div className="film__item">
            <div className="film__item-content">
                <span className="film__title">{film.title}</span>
                <div>
                    {!expanded && <button onClick={expandFilm} className="film__details">+</button>}
                    {expanded && <button onClick={hideFilm} className="film__details">-</button>}
                </div>
            </div>
            {expanded && <FilmDetails film={film} />}
        </div>
    )
}

export default Film