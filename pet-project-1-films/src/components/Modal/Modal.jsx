import React from 'react'
import './Modal.scss'
import {useSelector} from 'react-redux'

const Modal = (props) => {
    const { closeModal, closeButton, actions } = props
    
    const { info: action, data: item } = useSelector((state) => state.modal)
    
    return (
        <div data-testid='modalJest' className='modal-background' onClick={closeModal}>
            <div className='modal__content' onClick={(e) => e.stopPropagation()}>
                <header className='modal__content-header'>
                    <div>{action}?</div>
                    {closeButton && (
                        <div data-testid='closeButtonJest' onClick={closeModal} className='modal__close-btn'>
                            &times;
                        </div>
                    )}
                </header>
                <div>
                    <p className='modal__content-text'>{`Действительно ${action} ${item}?`}</p>
                    <div className='modal__buttons'>{actions}</div>
                </div>
            </div>
        </div>
    )
}

export default Modal
