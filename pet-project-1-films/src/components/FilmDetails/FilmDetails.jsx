import React, {useEffect} from "react"
import {useDispatch, useSelector} from "react-redux"
import {getFilmDetails, deleteFilm} from "../../store/operations"
import Loader from "../Loader/Loader"
import Modal from "../Modal/Modal";

const FilmDetails = (props) => {
    const { film } = props
    const { id, format, year } = props.film
    
    const filmDetails = useSelector((state) => state.filmDetails[id] || {})
    const isLoading = useSelector(state => !film && state.filmDetails.isLoading)
    const isOpen = useSelector((state) => state.modal.isOpen)
    
    const dispatch = useDispatch()

    const { actors = [] } = filmDetails
    
    const characterNames = actors.map(c => c.name).join(", ") || []
    
    useEffect(() => {
        dispatch(getFilmDetails(id))
    }, [id])
    
    if (isLoading) {
        return <Loader/>
    }
    
    const deleteFilmData = () => {
        dispatch(deleteFilm(id))
    }
    
    const showModal = (info, data) => {
        dispatch({ type: "SHOW_MODAL", payload: { info, data } })
    }
    
    const closeModal = () => {
        dispatch({type: "CLOSE_MODAL"})
    }
    
    return (
        <div className="film__info">
            <div>Year: {year}</div>
            <div>Format: {format}</div>
            <div>Actors: {characterNames}</div>
            <div className="film__delete-btn">
                <button onClick={() => {
                    showModal("Удалить ", props.film.title, "?")
                }}>Удалить</button>
            </div>
            {isOpen && (
                <Modal
                    closeButton={true}
                    closeModal={closeModal}
                    actions={[
                        <button
                            key={1}
                            onClick={() => {
                                deleteFilmData()
                                closeModal()
                            }}
                            className='modal-button'
                        >Ok</button>,
                        <button
                            key={2}
                            onClick={closeModal}
                            className='modal-button'
                        >Cancel</button>,
                    ]}
                />
            )}
        </div>
    )
}

export default FilmDetails