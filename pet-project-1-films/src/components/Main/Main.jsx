import React, {useState, useEffect} from "react"
import "./Main.scss"
import FilmForm from "../FilmForm/FimForm"
import Film from "../Film/Film"
import Loader from "../Loader/Loader"
import {connect, useDispatch, useSelector} from "react-redux"
import {createPages, getFilms, setCurrentPage} from "../../store/operations"

// const Main = ({ films = [], getFilms, isLoading }) => {
const Main = (props) => {
    const films = useSelector((state) => state.films)

    const [expanded, setExpanded] = useState(false)
    
    const dispatch = useDispatch()
    
    const currentPage = films.currentPage
    console.log(currentPage)
    const totalCount = films.meta?.total
    const perPage = films.perPage
    const pagesCount = Math.ceil(totalCount/perPage)
    const pages = []
    createPages(pages, pagesCount, currentPage)
    
    useEffect(() => {
        dispatch(getFilms(currentPage, perPage))
    }, [currentPage])

    // if (isLoading) {
    //     return <Loader/>
    // }
    
    const expandFilm = () => {
        setExpanded(true)
    }
    
    const hideFilm = () => {
        setExpanded(false)
    }
    
    console.log(films.data)
    
    const filmItems = films.data.map(f => <Film key={f.id} film={f}/>)
    
    const search = function (text) {
        const query = text.length > 1 ? `?search=${text}` : "?sort=title&order=ASC&limit=9&offset=0"
        fetch(`http://localhost:8000/api/v1/movies${query}`, {
            method: "get",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                Authorization: `${localStorage.getItem("webbyToken")}`
            },
        }).then((response) => {
                return response.json()
            }
        )
            .then(({ data }) => {
                dispatch({
                    type: "SEARCH_FILM",
                    payload: data,
                })
            })
    }
    
    return (
        <div className="main__container hoverable">
            <div className="main__add-film">
                <span className="main__title">Добавить фильм</span>
                <span className="main__expand">
                    {!expanded && <button onClick={expandFilm} className="film__details">+</button>}
                    {expanded && <button onClick={hideFilm} className="film__details">-</button>}
                </span>
                {expanded && <FilmForm/>}
            </div>
            <div className="main__title"><h1>Список Фильмов</h1></div>
            <hr className="main__line"></hr>
            <input
                type="search"
                className="main__search"
                placeholder="Search"
                onChange={(e) => {
                    search(e.target.value)
                }}
            />
            <div className="film__container">
                {filmItems.length > 0 ? filmItems : "empty data"}
            </div>
            <div className="main__pages-container">
                {pages.map((page, index) =>
                    // !console.log(page) &&
                    // !console.log(currentPage) &&
                    <span key={index} className={currentPage === page ? "main__current-page" : "main__page"}
                    onClick={() => dispatch(setCurrentPage(page))}>{page}</span>)}
            </div>
        </div>
    )
}
//
// const mapStateToProps = (state) => {
//     return {
//         films: state.films,
//         isLoading: state.films.isLoading
//     }
// }
//
// const mapDispatchToProps = (dispatch) => {
//     return {
//         getFilms: () => dispatch(getFilms())
//     }
// }
//
// export default connect(mapStateToProps, mapDispatchToProps)(Main)
export default Main
