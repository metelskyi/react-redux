import React from "react"
import {useDispatch} from "react-redux"
import {toast} from "react-toastify"
import "react-toastify/dist/ReactToastify.css"

toast.configure()

const Upload = () => {
    const dispatch = useDispatch()
    const uploadFile = (e) => {
        var formData = new FormData()
        formData.append("movies", e.target.files[0])
        
        fetch("http://localhost:8000/api/v1/movies/import", {
            method: "post",
            headers: {
                
                Authorization: `${localStorage.getItem("webbyToken")}`
            },
            body: formData
        }).then((response) => response.json())
            .then((data) => {
                console.log(data)
                
                if (data.status === 1) {
                    toast.success("Фильмы добавлены");
                    dispatch({
                        type: "ADD_FILM_FROM_FILE",
                        payload: data.data,
                    })
                } else if (data.status === 0) {
                    toast.error(data.error.code);
                } else {
                    return false
                }
            })
    }
    
    return (
        <div>
            <input type="file" onChange={uploadFile}/>
        </div>
    )
}

export default Upload