import React from "react"
import "./Header.scss"

const Header = () => {
    return (
        <div className="header hoverable">
            <div>
                <span className="header__logo">WEBBYLAB</span>
            </div>
            <div>
                <span className="header__films-text">Тестовое задание</span>
            </div>
        </div>
    )
}

export default Header
