import React from "react"
import "./FilmInput.scss"

const FilmInput = (props) => {
    const { type, placeholder, form, field } = props
    const { name } = field
    return (
        <div>
            <label>
                {form.errors[name] && form.touched[name] && (
                    <div className="input-error">{form.errors[name]}</div>
                )}
                <input placeholder={placeholder} type={type} {...field} />
            </label>
        </div>
    )
}

export default FilmInput
