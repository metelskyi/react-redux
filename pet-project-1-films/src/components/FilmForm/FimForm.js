import React from "react"
import {Form, Formik, Field} from "formik"
import * as Yup from "yup"
import "./FilmForm.scss"
import FilmInput from "./FilmInput"
import Upload from "../Upload/Upload"
import {useDispatch} from "react-redux"
import { toast } from "react-toastify"
import "react-toastify/dist/ReactToastify.css"

const validationSchema = Yup.object().shape({
    name: Yup.string().trim("Film name cannot include leading and trailing spaces").min(3, "Film Name needs to be at least 3 chars").required("Film Name is required"),
    year: Yup.number().integer("Film name must be and integer").min(1900, "Film Year must be from 1900 to 2021").max(2021, "Year must be from 1900 to 2021").required("Film Year is required(from 1900 to 2021)"),
    stars: Yup.string().trim("Stars cannot include leading and trailing spaces").matches(/[a-zA-Z -,]+$/g, "Film stars is not valid(available a-z, A-Z, - and ,").min(3, "Stars needs to be at least 3 chars").required("Film Stars is required"),
})

toast.configure()

const FilmForm = () => {
    const dispatch = useDispatch()
    
    const addFilm = (film) => {
        dispatch({
            type: "ADD_FILM",
            payload: film,
        })
    }
    
    const submitForm = (
        { name, year, format, stars },
        { resetForm }
    ) => {
        
        const film = {
            title: name,
            year: year,
            format: format,
            stars: stars.split(","),
        }
        
        resetForm({})
        
        fetch("http://localhost:8000/api/v1/movies", {
            method: "post",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                Authorization: `${localStorage.getItem("webbyToken")}`
            },
            body: JSON.stringify({
                "title": film.title,
                "year": film.year,
                "format": film.format,
                "actors": film.stars
            }),
        }).then((response) => response.json())
            .then((data) => {
                console.log(data.data)
                if (data.status === 1) {
                    addFilm(data.data)
                    toast.success("Film added");
                } else if (data.status === 0)
                    toast.error(data.error.code);
                else {
                    return false
                }
            })
    }
    
    return (
        <div className="form__container">
            <Formik
                initialValues={{
                    name: "",
                    year: "",
                    format: "VHS",
                    stars: "",
                }}
                onSubmit={submitForm}
                validationSchema={validationSchema}
                validateOnChange={false}
                validateOnBlur={false}
            >
                {() => {
                    return (
                        <div>
                            <Form>
                                <Field
                                    component={FilmInput}
                                    name="name"
                                    type="text"
                                    placeholder="Film Title"
                                />
                                <Field
                                    component={FilmInput}
                                    name="year"
                                    type="number"
                                    placeholder="Year (From 1900 to 2020)"
                                />
                                <Field className="form__format" name="format" component="select">
                                    <option value="VHS">VHS</option>
                                    <option value="DVD">DVD</option>
                                    <option value="Blu-Ray">Blu-Ray</option>
                                </Field>
                                <Field
                                    component={FilmInput}
                                    name="stars"
                                    type="string"
                                    placeholder="Stars">
                                </Field>
                                <button className="form-submit" type="submit">Добавить</button>
                            </Form>
                        </div>
                    )
                }}
            </Formik>
            <Upload/>
        </div>
    )
}

export default FilmForm
