const initialState = {
    films: {
        isLoading: true,
        data: [],
        currentPage: 1,
        perPage: 9,
    },
    filmDetails: {
        isLoading: true,
    },
    modal: {
        info: "",
        data: null,
        isOpen: false,
    },
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_FILM": {
            return { ...state, films: { ...state.films, data: [...state.films.data, action.payload] } }
        }
        case "ADD_FILM_FROM_FILE": {
            return { ...state, films: { ...state.films, data: [...state.films.data, ...action.payload] } }
        }
        case "SET_FILMS": {
            return { ...state, films: { ...state.films, ...action.payload } }
        }
        case "SET_FILMS_LOADING": {
            return { ...state, films: { ...state.films, isLoading: action.payload } }
        }
        case "SET_FILM_DETAILS": {
            return {
                ...state,
                filmDetails: {
                    ...state.filmDetails,
                    [action.payload.filmId]: action.payload.backData
                }
            }
        }
        case "SET_FILM_DETAILS_LOADING": {
            return { ...state, filmDetails: { ...state.filmDetails, isLoading: action.payload } }
        }
        case "DELETE_FILM": {
            const newData = state.films.data.filter(({ id }) => id !== action.payload)
            return {
                ...state,
                films: {
                    ...state.films,
                    data: newData
                }
            }
        }
        case "SEARCH_FILM": {
            return { ...state, films: { ...state.films, data: action.payload } }
        }
        case "SHOW_MODAL":
            return {
                ...state,
                modal: { ...state.modal, info: action.payload.info, data: action.payload.data, isOpen: true }
            }
        case "CLOSE_MODAL":
            return { ...state, modal: { ...state.modal, isOpen: false } }
        default: {
            return state
        }
        case "SET_CURRENT_PAGE": {
            return { ...state, films: { ...state.films, currentPage: action.payload }}
        }
    }
}

export default reducer;