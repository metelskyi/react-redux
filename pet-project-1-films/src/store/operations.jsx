import axios from 'axios';

export const createUser = () => {
    fetch("http://localhost:8000/api/v1/users", {
        method: "post",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            "email": "testerzz2@webbylab.com",
            "name": "testerzz2",
            "password": "webbylab",
            "confirmPassword": "webbylab"
        }),
    }).then((response) => {
            return response.json()
        }
    )
        .then((data) => {
            localStorage.setItem("webbyToken", data.token)
        })
        .then(() => {
            window.location.href='/';
        })
}

export const getFilms = (currentPage, perPage) => (dispatch) => {
    dispatch({ type: "SET_FILMS_LOADING", payload: true })
    axios(`http://localhost:8000/api/v1/movies?sort=title&order=ASC&limit=${perPage}&offset=${currentPage * perPage - perPage}`, {
        method: "get",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: `${localStorage.getItem("webbyToken")}`
        },
    })
        .then(res => {
            console.log(res)
            dispatch({ type: "SET_FILMS", payload: res.data })
            dispatch({ type: "SET_FILMS_LOADING", payload: false })
        })
}

export const getFilmDetails = (filmId) => (dispatch) => {
    dispatch({ type: "SET_FILM_DETAILS_LOADING", payload: true })
    axios(`http://localhost:8000/api/v1/movies/${filmId}`, {
        method: "get",
            headers: {
            "Content-Type": "application/json; charset=utf-8",
                Authorization: `${localStorage.getItem("webbyToken")}`
        },
    })
        .then(res => {
            dispatch({ type: "SET_FILM_DETAILS", payload: {backData:res.data.data, filmId } })
            dispatch({ type: "SET_FILM_DETAILS_LOADING", payload: false })
        })
}

export const deleteFilm = (payload) => (dispatch) => {
    dispatch({ type: "DELETE_FILM", payload })
    axios(`http://localhost:8000/api/v1/movies/${payload}`, {
        method: "delete",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            Authorization: `${localStorage.getItem("webbyToken")}`
        },
    })
}

export const setCurrentPage = (page) => (dispatch) => {
    dispatch({type:"SET_CURRENT_PAGE", payload:page})
}

export function createPages(pages, pagesCount, currentPage) {
    if (pagesCount > 10) {
        if (currentPage > 5) {
            for (let i = currentPage-4; i <= currentPage + 5; i++) {
                pages.push(i)
                if (i == pagesCount) break
            }
        } else {
            for (let i = 1; i <= 10; i++) {
                pages.push(i)
                if (i == pagesCount) break
            }
        }
    } else {
        for (let i = 1; i <= pagesCount; i++) {
            pages.push(i)
        }
    }
}