import './App.css'
import Header from './components/Header/Header'
import Main from './components/Main/Main'
import {createUser} from "./store/operations"
import React from "react"

function App() {
    return (
        <div className="App">
            {localStorage.getItem("webbyToken") === null
                ?
                <button className="start" onClick={createUser}>начать</button>
                :
                <>
                    <Header/>
                    <Main/>
                </>
            }
        </div>
    );
}

export default App
