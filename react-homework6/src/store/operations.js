import axios from 'axios'
import {SUCCESS} from './types'

export const getTrash = () => {
    const trash = localStorage.getItem('Trash')
    return trash ? JSON.parse(trash) : {}
}

export const setTrash = (data) => {
    localStorage.setItem('Trash', JSON.stringify(data))
}

export const deleteTrash = () => {
    localStorage.removeItem('Trash')
}

export const getFavorites = () => {
    const favouritesLS = localStorage.getItem('Favourite')
    return favouritesLS ? JSON.parse(favouritesLS) : []
}

export const setFavorites = (data) => {
    localStorage.setItem('Favourite', JSON.stringify(data))
}

export const loadItems = () => (dispatch) => {
    const favourites = getFavorites()
    const trash = getTrash()
    
    axios('./items.json').then((res) => {
        const newItems = res.data.map((item) => {
            item.favourite = favourites.includes(item.article)
            item.inTrashAmount = trash[item.article] || null
            return item
        })
        dispatch({ type: SUCCESS, payload: newItems })
    })
}