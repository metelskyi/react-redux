import React from 'react'
import PropTypes from 'prop-types'

const Button = ({ backgroundColor, text, onClick, className }) => {
    return (
        <button
            data-testid='button'
            className={className}
            style={{ backgroundColor: backgroundColor }}
            onClick={onClick}
        >
            {text}
        </button>
    )
}

export default Button

Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
}
