import {render, fireEvent} from '@testing-library/react'
import Modal from './Modal'
import userEvent from '@testing-library/user-event'

jest.mock('react-redux', () => ({
    useSelector: () => {
        return {info: 'testAction', data: {name: 'itemName'}}
    },
}))

const idModal = 'modalJest'
const idButton = 'closeButtonJest'

describe('Modal.js', () => {
    test('Render', () => {
        render(<Modal />)
    })
    
    test('Action', () => {
        const action = ['Action']
        render(<Modal actions={action} />)
    })
    
    test('Close Modal', () => {
        const closeModal = jest.fn()
        const {getByTestId} = render(<Modal closeModal={closeModal} />)
        userEvent.click(getByTestId(idModal))
        expect(closeModal).toHaveBeenCalled()
    })
    
    test('Close Button', () => {
        const closeModal = jest.fn()
        const {getByTestId} = render(
            <Modal closeModal={closeModal} closeButton />
        )
        userEvent.click(getByTestId(idButton))
        expect(closeModal).toHaveBeenCalled()
    })
    
    test('Class', () => {
        const className = 'modal-background'
        const {getByTestId} = render(
            <Modal className={className} />
        )
        expect(getByTestId(idModal)).toHaveClass(className)
    })
    
    test('stopPropagation', () => {
        const {getByTestId} = render(<Modal />)
        const modalBody = getByTestId('modalJest')
        expect(modalBody).toBeInTheDocument()
        fireEvent.click(modalBody)
    })
    
    test('Snapshot', () => {
        const { container } = render(<Modal />)
        expect(container.innerHTML).toMatchSnapshot()
    })
})