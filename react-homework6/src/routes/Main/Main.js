import React from 'react'
import Card from '../../components/Card/Card'
import './Main.scss'
import {useSelector} from 'react-redux'

const CardList = () => {
    const items = useSelector((state) => state.items.data)
    
    return (
        <>
            <div className='case-name'><h2>EQUINOX CRATE</h2></div>
            <hr className='case__line'></hr>
            <div className='container'>
                {items.map((item) => {
                    return <Card key={item.article} item={item} toTrash/>
                })}
            </div>
        </>
    )
}

export default CardList
