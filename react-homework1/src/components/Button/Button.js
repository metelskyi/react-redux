import React, {Component} from 'react'

export default class Button extends Component {
    render() {
        const { backgroundColor, text, onClick, className } = this.props
        return (
            <button
                className={className}
                style={{ backgroundColor: backgroundColor }}
                onClick={onClick}
            >
                {text}
            </button>
        )
    }
}
