import './App.css'

import React, { Component } from 'react'
import Button from './components/Button/Button'
import Modal from './components/Modal/Modal'

export default class App extends Component {
  constructor() {
    super()
    this.state = {
      firstModalDisplay: false,
      secondModalDisplay: false,
    }
  }
  
  openFirstModal = () => {
    this.setState({ firstModalDisplay: true })
  }
  
  closeFirstModal = () => {
    this.setState({ firstModalDisplay: false })
  }
  
  openSecondModal = () => {
    this.setState({ secondModalDisplay: true })
  }
  
  closeSecondModal = () => {
    this.setState({ secondModalDisplay: false })
  }
  
  render() {
    const { firstModalDisplay, secondModalDisplay } = this.state
    return (
        <div className='app__buttons'>
          <Button
              backgroundColor='red'
              text='Open first modal'
              className='modal-button'
              onClick={this.openFirstModal}
          />
          <Button
              backgroundColor='green'
              text='Open second modal'
              className='modal-button'
              onClick={this.openSecondModal}
          />
          {firstModalDisplay && (<Modal
              modalDisplay={firstModalDisplay}
              text='First modal some text'
              closeButton={true}
              close={this.closeFirstModal}
              header='First modal header'
              actions={[
                <Button
                    key={1}
                    onClick={this.closeFirstModal}
                    className='modal-button'
                    text='Ok'
                />,
                <Button
                    key={2}
                    onClick={this.closeFirstModal}
                    className='modal-button'
                    text='Cancel'
                />
              ]}
          />)}
          {secondModalDisplay && (<Modal
              modalDisplay={secondModalDisplay}
              text='Second modal some text'
              closeButton={true}
              close={this.closeSecondModal}
              header='Second modal header'
              actions={[
                <Button
                    key={3}
                    onClick={this.closeSecondModal}
                    className='modal-button'
                    text='neOk'
                />,
                <Button
                    key={4}
                    onClick={this.closeSecondModal}
                    className='modal-button'
                    text='neCancel'
                />
              ]}
          />)}
        </div>
    );
  }
}
