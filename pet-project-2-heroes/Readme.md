Front:
React + Redux,
react-router,
react-toastify,
scss + BEM.

Back:
express,
mongo, mongoose, mongoose-paginate,
cors,
postman.