import Schema from "./Schema.js";
import fileService from "./fileService.js";

class Service {
    async create(hero) {
        // const fileName = fileService.saveFile(img)
        const addedHero = await Schema.create(hero)
        return addedHero;
    }
    
    async getOne(id) {
        if (!id) {
            throw new Error("invalid id")
        }
        const hero = await Schema.findById(id);
        return hero;
    }
    
    async update(hero) {
        if (!hero._id) {
            throw new Error("invalid id")
        }
        const updated = await Schema.findByIdAndUpdate(hero._id, hero, { new: true });
        return updated
    }
    
    async delete(id) {
        if (!id) {
            throw new Error("invalid id")
        }
        const deleteOne = await Schema.findByIdAndDelete(id);
        return deleteOne
    }
}

export default new Service();