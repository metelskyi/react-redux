import express from 'express'
import mongoose from 'mongoose'
import router from "./Router.js";
import cors from "cors"
import fileUpload from "express-fileupload"

const PORT = 5000;
const DB_URL = "mongodb+srv://ninja:ninjatester@cluster0.xvwf6.mongodb.net/superheroes?retryWrites=true&w=majority"

const app = express()

app.use(cors());
app.options('*', cors());
app.use(express.json())
app.use(express.static("img"))
app.use(fileUpload({}))
app.use('/api', router)

app.get("/", (req, res) => {
    console.log(req.query)
    res.status(200).json("get ok")
})

async function startApp() {
    try {
        await mongoose.connect(DB_URL, { useUnifiedTopology: true, useNewUrlParser: true })
        app.listen(PORT, () => console.log("up on port " + PORT))
    } catch (e) {
        console.log(e)
    }
}

startApp()