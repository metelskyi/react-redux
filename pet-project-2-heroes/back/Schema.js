import mongoose from "mongoose";
import mongoosePaginate from "mongoose-paginate";

const Schema = new mongoose.Schema({
    nickname: {
        type: String,
        required: true,
        minlength:3,
    },
    real_name: {
        type: String,
        required: true,
        minlength:3,
    },
    origin_description: {
        type: String,
        required: true,
        minlength:3,
    },
    superpowers: {
        type: String,
        required: true,
        minlength:3,
    },
    catch_phrase: {
        type: String,
        required: true,
        minlength:3,
    },
    img: String
})

Schema.plugin(mongoosePaginate);

export default mongoose.model('Heroes', Schema)