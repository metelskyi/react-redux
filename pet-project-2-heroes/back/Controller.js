import Schema from "./Schema.js";
import Service from "./Service.js";

class Controller {
    async create(req, res) {
        try {
            const hero = await Service.create(req.body);
            res.json(hero)
        } catch (e) {
            res.status(500).json(e)
        }
    }
    
    async getAll(req, res) {
        try {
            const {page, perPage} = req.query;
            const options = {
                page: parseInt(page,10) || 1,
                limit: parseInt(perPage, 10) || 5
            }
            const getAll = await Schema.paginate({}, options);
            return res.json(getAll)
        } catch (e) {
            res.status(500).json(e)
        }
    }
    
    async getOne(req, res) {
        try {
            const getOne = await Service.getOne(req.params.id);
            return res.json(getOne)
        } catch (e) {
            res.status(500).json(e)
        }
    }
    
    async update(req, res) {
        try {
            const updated = await Service.update(req.body);
            return res.json(updated)
        } catch (e) {
            res.status(500).json(e)
        }
    }
    
    async delete(req, res) {
        try {
            const deleteOne = await Service.delete(req.params.id);
            return res.json(deleteOne)
        } catch (e) {
            res.status(500).json(e)
        }
    }
}

export default new Controller()