import Router from 'express'
import Controller from "./Controller.js";

const router = new Router()

router.post("/heroes", Controller.create)
router.get("/heroes", Controller.getAll)
router.get("/heroes/:id", Controller.getOne)
router.put("/heroes", Controller.update)
router.delete("/heroes/:id", Controller.delete)

export default router;
