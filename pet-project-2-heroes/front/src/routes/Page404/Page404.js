import React from "react"
import error404 from "./404.png"

const Page404 = () => {
  return (
    <div>
        <img src={error404} alt={"404"}/>
    </div>
  )
}

export default Page404
