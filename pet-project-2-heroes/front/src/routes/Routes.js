import React from "react"
import {Route, Switch} from "react-router-dom"
import Main from "./Main/Main"
import Page404 from "./Page404/Page404"
import AddHero from "./AddHero/AddHero";
import HeroInfo from "./HeroInfo/HeroInfo";
import EditHero from "./EditHero/EditHero";

const Routes = () => {
    return (
        <>
            <Switch>
                <Route exact path="/" component={Main}/>
                <Route exact path="/add" component={AddHero}/>
                <Route path="/hero/:id">
                    <HeroInfo/>
                </Route>
                <Route path="/edit/:id">
                    <EditHero/>
                </Route>
                <Route path="*" component={Page404}/>
            </Switch>
        </>
    )
}

export default Routes
