import React from "react"
import {Form, Formik, Field} from "formik"
import * as Yup from "yup"
import "./AddHero.scss"
import Input from "./Input"
import {useDispatch} from "react-redux"
import {toast} from "react-toastify"
import "react-toastify/dist/ReactToastify.css"

const validationSchema = Yup.object().shape({
    nickname: Yup.string().min(3, "Hero Name needs to be at least 3 chars").required("Hero Name is required"),
    real_name: Yup.string().min(3, "Hero Real Name needs to be at least 3 chars").required("Hero Real Name is required"),
    origin_description: Yup.string().min(3, "Origin Description needs to be at least 3 chars").required("Origin Description is required"),
    superpowers: Yup.string().min(3, "Superpowers needs to be at least 3 chars").required("Superpowers is required"),
    catch_phrase: Yup.string().min(3, "Catch Phrase needs to be at least 3 chars").required("Catch Phrase is required"),
})

toast.configure()

const AddHero = () => {
    const dispatch = useDispatch()
    
    const addHero = (hero) => {
        dispatch({
            type: "ADD_HERO",
            payload: hero,
        })
    }
    
    // const uploadFile = (e) => {
    //     var formData = new FormData()
    //     formData.append("movies", e.target.files[0])
    //     return formData
    // }
    
    const submitForm = (
        { nickname, real_name, origin_description, superpowers, catch_phrase },
        { resetForm }
    ) => {
        
        const hero = {
            nickname: nickname,
            real_name: real_name,
            origin_description: origin_description,
            superpowers: superpowers,
            catch_phrase: catch_phrase,
        }
        
        resetForm({})
        
        fetch("http://localhost:5000/api/heroes", {
            method: "post",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                "nickname": hero.nickname,
                "real_name": hero.real_name,
                "origin_description": hero.origin_description,
                "superpowers": hero.superpowers,
                "catch_phrase": hero.catch_phrase,
            }),
        }).then((response) => response.json())
            .then((data) => {
                // console.log("Hero add data", data)
                if (data) {
                    addHero(data)
                    toast.success("Hero added");
                } else if (data.errors) {
                    toast.error(data._message);
                }
                else {
                    return false
                }
            })
    }
    
    return (
        <div className="form__container">
            <Formik
                initialValues={{
                    nickname: "",
                    real_name: "",
                    origin_description: "",
                    superpowers: "",
                    catch_phrase: "",
                }}
                onSubmit={submitForm}
                validationSchema={validationSchema}
                validateOnChange={false}
                validateOnBlur={false}
            >
                {() => {
                    return (
                        <div>
                            <Form>
                                <Field
                                    component={Input}
                                    name="nickname"
                                    type="text"
                                    placeholder="Nick Name"
                                />
                                <Field
                                    component={Input}
                                    name="real_name"
                                    type="text"
                                    placeholder="Real Name"
                                />
                                <Field
                                    component={Input}
                                    name="origin_description"
                                    type="text"
                                    placeholder="Origin Description"
                                />
                                <Field
                                    component={Input}
                                    name="superpowers"
                                    type="text"
                                    placeholder="Super Powers"
                                />
                                <Field
                                    component={Input}
                                    name="catch_phrase"
                                    type="text"
                                    placeholder="Catch Phrase"
                                />
                                {/*<div>*/}
                                {/*    <input type="file" onChange={uploadFile}/>*/}
                                {/*</div>*/}
                                <button className="form-submit" type="submit">Add Hero</button>
                            </Form>
                        </div>
                    )
                }}
            </Formik>
        </div>
    )
}

export default AddHero
