import React, {useEffect} from "react"
import {Form, Formik, Field} from "formik"
import * as Yup from "yup"
import "./EditHero.scss"
import Input from "./Input"
import {useDispatch, useSelector} from "react-redux"
import {toast} from "react-toastify"
import "react-toastify/dist/ReactToastify.css"
import {useParams} from "react-router-dom";
import {getHeroDetails} from "../../store/operations";

const validationSchema = Yup.object().shape({
    nickname: Yup.string().min(3, "Hero Name needs to be at least 3 chars").required("Hero Name is required"),
    real_name: Yup.string().min(3, "Hero Real Name needs to be at least 3 chars").required("Hero Real Name is required"),
    origin_description: Yup.string().min(3, "Origin Description needs to be at least 3 chars").required("Origin Description is required"),
    superpowers: Yup.string().min(3, "Superpowers needs to be at least 3 chars").required("Superpowers is required"),
    catch_phrase: Yup.string().min(3, "Catch Phrase needs to be at least 3 chars").required("Catch Phrase is required"),
})

toast.configure()

const EditHero = () => {
    const dispatch = useDispatch()
    let { id } = useParams();
    const heroDetails = useSelector((state) => state.heroDetails[id] || {})
    const { nickname, real_name, origin_description, superpowers, catch_phrase } = heroDetails
    console.log("asdasd", heroDetails)
    
    useEffect(() => {
        dispatch(getHeroDetails(id))
    }, [id])
    
    
    const editHeroData = (hero) => {
        dispatch({
            type: "EDIT_HERO",
            payload: hero,
        })
    }
    
    const submitForm = (
        { nickname, real_name, origin_description, superpowers, catch_phrase },
        { resetForm }
    ) => {
        
        const hero = {
            nickname: nickname,
            real_name: real_name,
            origin_description: origin_description,
            superpowers: superpowers,
            catch_phrase: catch_phrase,
        }
        
        resetForm({})
        
        fetch("http://localhost:5000/api/heroes", {
            method: "put",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                "_id": id,
                "nickname": hero.nickname,
                "real_name": hero.real_name,
                "origin_description": hero.origin_description,
                "superpowers": hero.superpowers,
                "catch_phrase": hero.catch_phrase,
            }),
        }).then((response) => response.json())
            .then((data) => {
                console.log("Hero edit data", data)
                if (data) {
                    editHeroData(data)
                    toast.success("Updated");
                } else if (data.errors) {
                    toast.error(data._message);
                }
                else {
                    return false
                }
            })
    }
    
    return (
        <div className="form__container">
            <div className="form__edit">Edit Hero id: {id}</div>
            <Formik
                initialValues={{
                    nickname: "",
                    real_name: "",
                    origin_description: "",
                    superpowers: "",
                    catch_phrase: "",
                }}
                onSubmit={submitForm}
                validationSchema={validationSchema}
                validateOnChange={false}
                validateOnBlur={false}
            >
                {() => {
                    return (
                        <div>
                            <Form>
                                <Field
                                    component={Input}
                                    name="nickname"
                                    type="text"
                                    placeholder={nickname}
                                />
                                <Field
                                    component={Input}
                                    name="real_name"
                                    type="text"
                                    placeholder={real_name}
                                />
                                <Field
                                    component={Input}
                                    name="origin_description"
                                    type="text"
                                    placeholder={origin_description}
                                />
                                <Field
                                    component={Input}
                                    name="superpowers"
                                    type="text"
                                    placeholder={superpowers}
                                />
                                <Field
                                    component={Input}
                                    name="catch_phrase"
                                    type="text"
                                    placeholder={catch_phrase}
                                />
                                {/*<div>*/}
                                {/*    <input type="file" onChange={uploadFile}/>*/}
                                {/*</div>*/}
                                <button className="form-submit" type="submit">Edit Hero</button>
                            </Form>
                        </div>
                    )
                }}
            </Formik>
        </div>
    )
}

export default EditHero
