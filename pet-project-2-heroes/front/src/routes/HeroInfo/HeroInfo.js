import React, { useEffect } from "react";
import "./HeroInfo.scss";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import {getHeroDetails} from "../../store/operations"
import img from "./1.jpg"

const HeroInfo = () => {
    let { id } = useParams();

    const isLoading = useSelector(state => state.heroDetails.isLoading)
    const heroDetails = useSelector((state) => state.heroDetails[id] || {})
    const { nickname, real_name, origin_description, superpowers, catch_phrase } = heroDetails
    
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(getHeroDetails(id))
    }, [id])
    
    if (isLoading) {
        return <div className="App">Loading... Don`t go anywhere</div>
    }
    
    return (
        <div className="info__container">
            <img className="info__img" src={img} alt=""/>
            <div className="info__characteristics">
                <span className="info__characteristics__text-grey">Nick Name</span>
                <span className="info__characteristics__text-black">{nickname}</span>
                <span className="info__characteristics__text-grey">Real Name</span>
                <span className="info__characteristics__text-black">{real_name}</span>
                <span className="info__characteristics__text-grey">Origin Description</span>
                <span className="info__characteristics__text-black">{origin_description}</span>
                <span className="info__characteristics__text-grey">Super Powers</span>
                <span className="info__characteristics__text-black">{superpowers}</span>
                <span className="info__characteristics__text-grey">Catch Phrase</span>
                <span className="info__characteristics__text-black">{catch_phrase}</span>
            </div>
        </div>
    );
};

export default HeroInfo;
