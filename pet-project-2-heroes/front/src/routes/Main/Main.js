import React, {useEffect} from "react"
import Card from "../../components/Card/Card"
import "./Main.scss"
import {useDispatch, useSelector} from "react-redux"

import {loadHeroes, createPages, setCurrentPage} from "../../store/operations"

const Main = () => {
    const isLoading = useSelector((state) => state.heroes.isLoading)
    const heroes = useSelector((state) => state.heroes.docs)
    // console.log("heroes", heroes)
    const heroPages = useSelector((state) => state.heroes)
    const currentPage = heroPages.page
    const totalCount = heroPages.total
    const perPage = heroPages.limit
    const pagesCount = Math.ceil(totalCount / perPage)
    const pages = []
    createPages(pages, pagesCount, currentPage)
    
    const dispatch = useDispatch()
    
    useEffect(() => {
        dispatch(loadHeroes(currentPage, perPage))
    }, [currentPage])
    
    if (isLoading) {
        return <div className="App">Loading... Don`t go anywhere</div>
    }
    
    return (
        <>
            <div className="main__case-name"><h2>HEROES</h2></div>
            <hr className="main__case__line"></hr>
            <div className="main__container">
                {heroes.map((hero) => {
                    return <Card key={hero._id} hero={hero}/>
                })}
            </div>
            <div className="main__pages-container">
                {pages.map((page, index) =>
                    // !console.log("page", page) &&
                    // !console.log("currentPage", currentPage) &&
                    <span key={index} className={currentPage === page ? "main__current-page" : "main__page"}
                          onClick={() => dispatch(setCurrentPage(page))}>{page}</span>)}
            </div>
        </>
    )
}

export default Main
