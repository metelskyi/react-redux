const initialState = {
    heroes: {
        isLoading: true,
        docs: [],
    },
    heroDetails: {
        isLoading: true,
    },
    modal: {
        info: "",
        data: null,
        isOpen: false,
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_HEROES_LOADING": {
            return { ...state, heroes: { ...state.heroes, isLoading: action.payload } }
        }
        case "SET_HEROES": {
            return { ...state, heroes: action.payload }
        }
        case "ADD_HERO": {
            return { ...state, heroes: { ...state.heroes, docs: [...state.heroes.docs, action.payload] } }
        }
        case "DELETE_HERO": {
            const newData = state.heroes.docs.filter(({ _id }) => _id !== action.payload)
            return { ...state, heroes: { ...state.heroes, docs: newData } }
        }
        case "SET_CURRENT_PAGE": {
            return { ...state, heroes: { ...state.heroes, page: action.payload } }
        }
        case "SHOW_MODAL":
            return {
                ...state,
                modal: { ...state.modal, info: action.payload.info, data: action.payload.data, isOpen: true }
            }
        case "CLOSE_MODAL":
            return { ...state, modal: { ...state.modal, isOpen: false } }
        case "SET_HERO_DETAILS": {
            return {
                ...state, heroDetails: { ...state.heroDetails, [action.payload.heroId]: action.payload.backData }
            }
        }
        case "SET_HERO_DETAILS_LOADING": {
            return { ...state, heroDetails: { ...state.heroDetails, isLoading: action.payload } }
        }
        case "EDIT_HERO": {
            return { ...state, heroDetails: action.payload }
        }
        default:
            return state
    }
}

export default reducer;