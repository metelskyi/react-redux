import axios from "axios";
import {toast} from "react-toastify"
import "react-toastify/dist/ReactToastify.css"
toast.configure()

export const loadHeroes = (currentPage, perPage) => (dispatch) => {
    dispatch({ type: "SET_HEROES_LOADING", payload: true })
    axios(`http://localhost:5000/api/heroes?perPage=${perPage}&page=${currentPage++}`, {
        method: "get",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
    })
        .then(res => {
            console.log("res", res.data)
            dispatch({ type: "SET_HEROES", payload: res.data })
            dispatch({ type: "SET_HEROES_LOADING", payload: false })
        })
}

export const deleteHero = (payload) => (dispatch) => {
    dispatch({ type: "DELETE_HERO", payload })
    axios(`http://localhost:5000/api/heroes/${payload}`, {
        method: "delete",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
    })
        .then(res => {
            console.log("res", res)
            if (res) {
                toast.success("Hero deleted");
            }
        }).catch(
        function (error) {
            toast.error("Error");
            return Promise.reject(error)
        }
    )
}

export const setCurrentPage = (page) => (dispatch) => {
    dispatch({type:"SET_CURRENT_PAGE", payload:page})
}

export function createPages(pages, pagesCount, currentPage) {
    if (pagesCount > 10) {
        if (currentPage > 5) {
            for (let i = currentPage-4; i <= currentPage + 5; i++) {
                pages.push(i)
                if (i === pagesCount) break
            }
        } else {
            for (let i = 1; i <= 10; i++) {
                pages.push(i)
                if (i === pagesCount) break
            }
        }
    } else {
        for (let i = 1; i <= pagesCount; i++) {
            pages.push(i)
        }
    }
}

export const getHeroDetails = (heroId) => (dispatch) => {
    dispatch({ type: "SET_HERO_DETAILS_LOADING", payload: true })
    axios(`http://localhost:5000/api/heroes/${heroId}`, {
        method: "get",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
    })
        .then(res => {
            console.log(res.data)
            dispatch({ type: "SET_HERO_DETAILS", payload: {backData:res.data, heroId } })
            dispatch({ type: "SET_HERO_DETAILS_LOADING", payload: false })
        })
}