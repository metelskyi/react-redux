import React from 'react'
import {NavLink} from "react-router-dom"
import "./Header.scss"

const Header = () => {
    return (
        <div className="header">
            <div>
                <NavLink to="/">
                    <span className="logo">JS ninjas</span>
                </NavLink>
            </div>
            <div>
                <NavLink to="/add">
                    <span className="nav__text">ADD HERO</span>
                </NavLink>
            </div>
        </div>
    )
}

export default Header
