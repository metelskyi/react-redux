import React from "react"
import "./Card.scss"
import {useDispatch, useSelector} from "react-redux"
import Modal from "../Modal/Modal";
import {deleteHero} from "../../store/operations"
import img from "./img.jpg"
import edit from "./edit.png"

const Card = ({ hero }) => {
    const { nickname, _id } = hero
    const isOpen = useSelector((state) => state.modal.isOpen)
    const dispatch = useDispatch()
    
    const deleteHeroData = () => {
        dispatch(deleteHero(_id))
    }
    
    const showModal = (info, data) => {
        dispatch({ type: "SHOW_MODAL", payload: { info, data } })
    }
    
    const closeModal = () => {
        dispatch({type: "CLOSE_MODAL"})
    }

    return (
        <div className="card__container" style={{ backgroundColor: "white" }}>
            <div className="card__name">
                <span className="card__link" onClick={() => {
                    window.location.href = `/hero/${_id}`;
                }}>{nickname}</span>
            </div>
            <img className="card__edit" src={edit} alt="Edit alt" onClick={() => {
                window.location.href = `/edit/${_id}`;
            }}/>
            <button className="card__delete" onClick={() => {
                showModal("Удалить ", nickname, "?")
            }}>X</button>
            <img className="card__img" src={img} alt="Img alt" onClick={() => {
                window.location.href = `/hero/${_id}`;
            }}/>
            {isOpen && (
                <Modal
                    closeButton={true}
                    closeModal={closeModal}
                    actions={[
                        <button
                            key={1}
                            onClick={() => {
                                deleteHeroData()
                                closeModal()
                            }}
                            className="modal-button"
                        >Ok</button>,
                        <button
                            key={2}
                            onClick={closeModal}
                            className="modal-button"
                        >Cancel</button>,
                    ]}
                />
            )}
        </div>
    )
}

export default Card