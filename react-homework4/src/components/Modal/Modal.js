import React from 'react'
import './Modal.scss'
import PropTypes from 'prop-types'
import {useSelector} from 'react-redux'

const Modal = (props) => {
    const { closeModal, closeButton, actions } = props
    
    const { info: action, data: item } = useSelector((state) => state.modal)

    return (
        <div className='modal-background' onClick={closeModal}>
            <div className='modal__content' onClick={(e) => e.stopPropagation()}>
                <header className='modal__content-header'>
                    <div>{action}</div>
                    {closeButton && (
                        <div onClick={closeModal} className='modal__close-btn'>
                            &times;
                        </div>
                    )}
                </header>
                <div>
                    <p className='modal__content-text'>{`${action} ${item.name}?`}</p>
                    <div className='modal__buttons'>{actions}</div>
                </div>
            </div>
        </div>
    )
}

export default Modal

Modal.propTypes = {
    closeModal: PropTypes.func,
    closeButton: PropTypes.bool,
    actions: PropTypes.array,
}
