import React from 'react'
import './Card.scss'
import {hearth} from '../../themes/icons'
import Button from '../Button/Button'
import PropTypes from 'prop-types'
import price from './price.png'
import {useDispatch} from 'react-redux'
import {SHOW_MODAL, ADD_REMOVE_FAVORITE} from '../../store/types'

const Card = ({ item, toTrash, fromTrash }) => {
    const { name, path, color, article, favourite } = item
    
    const dispatch = useDispatch()
    
    const addToFavourite = (id) => {
        dispatch({ type: ADD_REMOVE_FAVORITE, payload: id })
    }
    
    const showModal = (info, data) => {
        dispatch({ type: SHOW_MODAL, payload: { info, data } })
    }
    
    return (
        <div className='card-container' style={{ backgroundColor: color }}>
            <div className='card-name'>
                <h3>{name}</h3>
            </div>
            <div className='card-price'>
                <img style={{ maxHeight: 20, marginRight: 3, marginTop: 3, marginBottom: 2 }} src={price} alt='price'/>
                <span>1920</span>
            </div>
            <img src={path} alt='Img alt'/>
            {toTrash && (
                <Button
                    className='card-add-button'
                    onClick={() => {
                        showModal('Add to trash', item)
                    }}
                    text={'Add to trash'}
                />
            )}
            {fromTrash && (
                <div
                    onClick={() => {
                        showModal('Remove from trash', item)
                    }}
                    className='close-btn'>
                    &times;
                </div>
            )}
            <div onClick={() => addToFavourite(article)}>
                {hearth(favourite)}
            </div>
        </div>
    )
}

export default Card

Card.propTypes = {
    item: PropTypes.shape({
        name: PropTypes.string,
        path: PropTypes.string,
        article: PropTypes.string,
        color: PropTypes.string,
        favourite: PropTypes.bool,
    }),
    toTrash: PropTypes.bool,
    fromTrash: PropTypes.bool,
}
