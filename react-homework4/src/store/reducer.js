import {
    SUCCESS,
    SHOW_MODAL,
    ADD_REMOVE_FAVORITE,
    CLOSE_MODAL,
    ADD_TO_TRASH,
    REMOVE_FROM_TRASH
} from './types'
import {getTrash, setTrash, getFavorites, setFavorites} from './operations'

const initialState = {
    items: {
        data: [],
        isLoading: true,
    },
    modal: {
        info: '',
        data: null,
        isOpen: false,
    },
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SUCCESS:
            return {
                ...state,
                items: { ...state.data, data: action.payload, isLoading: false },
            }
        case SHOW_MODAL:
            return {
                ...state,
                modal: {
                    ...state.modal,
                    info: action.payload.info,
                    data: action.payload.data,
                    isOpen: true,
                },
            }
        case CLOSE_MODAL:
            return { ...state, modal: { ...state.modal, isOpen: false } }
        case ADD_REMOVE_FAVORITE:
            const newArray = state.items.data.map((item) => {
                if (item.article === action.payload) {
                    item.favourite = !item.favourite
                    
                    const favourites = getFavorites()
                    if (favourites.includes(item.article)) {
                        favourites.splice(favourites.indexOf(item.article), 1)
                    } else {
                        favourites.push(item.article)
                    }
                    setFavorites(favourites)
                }
                return item
            })
            return { ...state, items: { ...state.items, data: newArray } }
        case ADD_TO_TRASH:
            const newItems = state.items.data.map((item) => {
                if (item.article === action.payload) {
                    item.inTrashAmount++
                    
                    const trash = getTrash()
                    if (Object.keys(trash).includes(item.article)) {
                        trash[item.article]++
                    } else {
                        trash[item.article] = 1
                    }
                    setTrash(trash)
                }
                return item
            })
            return { ...state, items: { ...state.items, data: newItems } }
        case REMOVE_FROM_TRASH:
            const newItems2 = state.items.data.map((item) => {
                if (item.article === action.payload) {
                    item.inTrashAmount--
                    
                    const trash = getTrash()
                    trash[item.article]--
                    if (trash[item.article] === 0) {
                        delete trash[item.article]
                    }
                    setTrash(trash)
                }
                return item
            })
            return {
                ...state,
                items: { ...state.items, data: newItems2 },
            }
        default:
            return state
    }
}

export default reducer
