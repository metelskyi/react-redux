import React from 'react'
import './Card.scss'
import {hearth} from '../../themes/icons'
import Button from '../Button/Button'
import PropTypes from 'prop-types'

const Card = (props) => {
    const {
        item,
        saveThisItem,
        openModal,
        addToFavourite,
        favourite,
        toTrash,
    } = props
    const { name, path, color, article } = item
    
    return (
        <div className='card-container' style={{ backgroundColor: color }}>
            <img src={path} alt='Img alt'/>
            <div>
                <b className='card-name'>{name}</b>
                <span className='favourite' onClick={() => addToFavourite(article)}>
                        {hearth(favourite)}
                    </span>
            </div>
            <b>50₴</b>
            {toTrash && (
                <Button
                    className='card-add-button'
                    onClick={() => {
                        saveThisItem(item)
                        openModal('Add to cart')
                    }}
                    text={'Add to cart'}
                />
            )}
        </div>
    )
}

export default Card

Card.propTypes = {
    item: PropTypes.shape({
        name: PropTypes.string,
        path: PropTypes.string,
        article: PropTypes.string,
        color: PropTypes.string,
    }),
}
