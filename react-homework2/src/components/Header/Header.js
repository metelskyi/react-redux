import React from 'react'
import './Header.scss'
import logo from './logo.png'

const Header = () => {
    return (
        <>
            <div className='header'><a href='/main'><img className='logo' src={logo} href='/main' alt={'logo'}/></a></div>
        </>
    )
}

export default Header
