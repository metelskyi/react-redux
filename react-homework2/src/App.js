import './App.scss'

import React, {useEffect, useState} from 'react'
import axios from 'axios'
import Button from './components/Button/Button'
import Header from './components/Header/Header'
import Modal from './components/Modal/Modal'
import Routes from './routes/Routes'

const favouritesFromLS = () => {
    const favouritesLS = localStorage.getItem('Favourite')
    return favouritesLS ? JSON.parse(favouritesLS) : []
}

const App = () => {
    const [isLoading, setIsLoading] = useState(true)
    const [items, setItems] = useState([])
    const [thisItem, setThisItem] = useState({})
    const [showModal, setShowModal] = useState(false)
    const [favorites, setFavorites] = useState(favouritesFromLS())
    
    const saveThisItem = (item) => {
        setThisItem(item)
    }
    
    const openModal = () => {
        setShowModal(true)
    }
    
    const closeModal = () => {
        setShowModal(false)
    }
    
    const addToCart = (article) => {
        if (localStorage.getItem('Cart')) {
            const cart = JSON.parse(localStorage.getItem('Cart'))
            
            if (Object.keys(cart).includes(article)) {
                cart[article]++
            } else {
                cart[article] = 1
            }
            
            localStorage.setItem('Cart', JSON.stringify(cart))
        } else {
            const cart = {}
            cart[article] = 1
            localStorage.setItem('Cart', JSON.stringify(cart))
        }
        closeModal()
    }
    
    const addToFavourite = (article) => {
        let newFavorites
        if (favorites.includes(article)) {
            newFavorites = favorites.filter((favorite) => {
                return favorite !== article
            })
        } else {
            newFavorites = [...favorites, article]
        }
        setFavorites(newFavorites)
        localStorage.setItem('Favourite', JSON.stringify(newFavorites))
    }
    
    useEffect(() => {
        axios('./items.json').then((res) => {
            res.data.forEach((card) => {
                card.favourite =
                    localStorage.getItem('Favourite') &&
                    JSON.parse(localStorage.getItem('Favourite')).includes(card.article)
            })
            setItems(res.data)
            setIsLoading(false)
        })
    }, [])
    
    if (isLoading) {
        return <div className='App'>Loading... Don`t go anywhere</div>
    }
    
    return (
        <div className='App'>
            <Header>
            </Header>
            <Routes
                items={items}
                favorites={favorites}
                saveThisItem={saveThisItem}
                openModal={openModal}
                addToFavourite={addToFavourite}
            />
            {showModal && (
                <Modal
                    text={`Add to cart '${thisItem.name}'?`}
                    closeButton={true}
                    closeButtonOnClick={closeModal}
                    header={`'${thisItem.name}' available`}
                    actions={[
                        <Button
                            key={1}
                            onClick={() => {
                                addToCart(thisItem.article)
                            }}
                            className='modal-button'
                            text='Add'
                        />,
                        <Button
                            key={2}
                            onClick={closeModal}
                            className='modal-button'
                            text='Cancel'
                        />,
                    ]}
                />
            )}
        </div>
    )
}

export default App
