import React from 'react'
import {NavLink} from 'react-router-dom'
import './Header.scss'

const Header = () => {
    return (
        <div className='header'>
            <div>
                <NavLink to='/main'>
                    <span className='logo'>PUBG SKINS</span>
                </NavLink>
            </div>
            <div>
                <NavLink to='/favourites'>
                    <span className='nav__text'>FAVOURITES</span>
                </NavLink>
                <NavLink to='/trash'>
                    <span className='nav__text'>TRASH</span>
                </NavLink>
            </div>
        </div>
    )
}

export default Header
