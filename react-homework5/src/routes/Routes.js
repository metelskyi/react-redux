import React from 'react'
import {Route, Redirect, Switch} from 'react-router-dom'
import Main from './Main/Main'
import Trash from './Trash/Trash'
import Favourites from './Favourites/Favourites'
import Page404 from './Page404/Page404'

const Routes = () => {
    return (
        <>
            <Switch>
                <Redirect exact from='/' to='/main' />
                <Route exact path='/main' component={Main} />
                <Route exact path='/favourites' component={Favourites} />
                <Route exact path='/trash' component={Trash} />
                <Route path='*' component={Page404} />
            </Switch>
        </>
    )
}

export default Routes
