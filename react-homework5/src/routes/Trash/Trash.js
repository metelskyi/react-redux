import React from 'react'
import './Trash.scss'
import Card from '../../components/Card/Card'
import { getTrash } from '../../store/operations'
import { useSelector } from 'react-redux'
import TrashForm from '../../components/TrashForm/TrashForm'

const Trash = () => {
    const items = useSelector((state) => state.items.data)
    const trash = items.filter((item) => item.inTrashAmount)
    
    if (!getTrash()) {
        return <div className='container'>Empty</div>
    }
    
    return (
        <div className='container'>
            {trash.map((item) => {
                return <Card key={item.article} item={item} fromTrash />
            })}
            <TrashForm />
        </div>
    )
}

export default Trash
